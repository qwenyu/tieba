package com.example.teiba;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.teiba.mapper.*;
import com.example.teiba.pojo.*;
import org.junit.jupiter.api.Test;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
@MapperScan("com.example.teiba")
class TeibaApplicationTests {

    @Autowired
    private UserMapper usermapper;
    @Autowired
    private ArticleMapper articleMapper;

    @Autowired
    private FavorMapper favorMapper;

    @Autowired
    private CommentMapper commentMapper;

    @Autowired
    private CommentFavorMapper commentFavorMapper;

    @Test
    void contextLoads() {
//        查询所有用户
        List<User> list= usermapper.findAlluser();
        for (User comment : list) {
            System.out.println(comment);
        }
    }
    @Test
    void contextLoads2() {
//        查询所有文章
        List<Article> list= articleMapper.findAllarticle();
        for (Article comment : list) {
            System.out.println(comment);
        }
    }
    @Test
    void contextLoads3() {
        Article artcle=new Article();
        artcle.setArticleId(null);
        artcle.setArtContent("hahahahahahahaha");
        artcle.setTitle("wulexi");
        artcle.setArtLikes(0);
        artcle.setDeleted(0);
        artcle.setViews(0);
        artcle.setUserId(1);
        artcle.setTag("1");

//        插入信息
        articleMapper.insert(artcle);
        contextLoads2();
    }
    @Test
    void contextLoads4() {
//        查询点赞和观看
        Map<String, Integer> result= articleMapper.likesView(1);
        int likes = (Integer) result.get("artLikes");
        int views = (Integer) result.get("views");
        System.out.println(likes);
        System.out.println(views);
    }

    @Test
    public void test5(){
        QueryWrapper<Favor> likeQueryWrapper = new QueryWrapper<>();
        likeQueryWrapper.eq("articleId", 12);
        List<Favor> likes = favorMapper.selectList(likeQueryWrapper);
    }

    @Test
    public void test6(){
        QueryWrapper<Commentfavor> likeQueryWrapper = new QueryWrapper<>();
        likeQueryWrapper.eq("commentId", 8);
        commentFavorMapper.selectList(likeQueryWrapper);
    }

    @Test
    public void test7(){
        List<Article> articles = articleMapper.findDeleted();
        HashMap<Integer, String> map1 = new HashMap<>();

        for (Article article :articles) {
            User user = usermapper.selectById(article.getUserId());
            map1.put(article.getArticleId(), user.getUsername());
        }

        for (Article article :articles) {
            System.out.println("article = " + article);
        }


        for (Integer key : map1.keySet()) {
            String value = map1.get(key);
            // 处理键和对应的值
            System.out.println(key + value);
        }
    }

    @Test
    public void test8(){
        articleMapper.update_deleted(6);
    }

    @Test
    public void test9(){
        Article id = articleMapper.findId(3);
        System.out.println(id);
    }

    @Test
    public void test10(){
        List<Comment> records = commentMapper.Page(1, 8);
        List<Article> asd = articleMapper.findDeletedLike("人工智能技");
    }

    @Test
    public void test11(){
        usermapper.deleteById(1);
    }
    @Test
    public void test12(){
        int uid = 1;

        IPage<Article> page = new Page<>(1, 8);


//       根据ID查找
        QueryWrapper<Article> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userId", uid);

        queryWrapper.orderByDesc("create_time");

        IPage<Article> articles_page = articleMapper.selectPage(page, queryWrapper);
        List<Article> articles = articles_page.getRecords();
    }

}
