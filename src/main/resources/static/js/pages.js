var totalPages = [[${totalPages}]]; // 总页数
var currentPage = [[${currentPage}]]; // 当前页数

function createPagination() {
    var pagination = document.getElementById('pagination');
    pagination.innerHTML = '';

    // 添加上一页按钮
    if (currentPage > 1) {
        var prevBtn = document.createElement('button');
        prevBtn.innerText = '上一页';
        prevBtn.addEventListener('click', gotoPreviousPage);
        pagination.appendChild(prevBtn);
    }

    // 添加数字页码
    for (var i = 1; i <= totalPages; i++) {
        var pageBtn = document.createElement('button');
        pageBtn.innerText = i;
        pageBtn.addEventListener('click', gotoPage.bind(null, i));
        if (i === currentPage) {
            pageBtn.classList.add('active');
        }
        pagination.appendChild(pageBtn);
    }

    // 添加下一页按钮
    if (currentPage < totalPages) {
        var nextBtn = document.createElement('button');
        nextBtn.innerText = '下一页';
        nextBtn.addEventListener('click', gotoNextPage);
        pagination.appendChild(nextBtn);
    }
}

function gotoPage(page) {
    if (page < 1 || page > totalPages) {
        return;
    }
    currentPage = page;
    createPagination();
    // TODO: 在此处执行更新当前页内容的操作
}

function gotoPreviousPage() {
    gotoPage(currentPage - 1);
}

function gotoNextPage() {
    gotoPage(currentPage + 1);
}

// 初始化分页器
createPagination();