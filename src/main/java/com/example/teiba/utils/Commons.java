package com.example.teiba.utils;

import com.example.teiba.mapper.ArticleMapper;
import com.example.teiba.mapper.UserMapper;
import com.example.teiba.pojo.Article;
import com.example.teiba.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.text.SimpleDateFormat;
import java.util.Date;
@Component
public class Commons {

    @Autowired
    private  UserMapper usermapper;

    @Autowired
    private ArticleMapper articleMapper;

    //把Date格式化成 yyyy-mm-dd
    public static String dateFormat(Date created){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String format = sdf.format(created);
        return format;
    }

    public  String get_username(int uid){
        User user = usermapper.selectById(uid);
        return user.getUsername();
    }
    public String get_header(int uid){
        User user = usermapper.selectById(uid);
        return user.getHeader();
    }

    public  String get_articleTitle(int articleId){
        Article article = articleMapper.selectById(articleId);
        return article.getTitle();
    }
//    只取前两个p标签
    public String get_twoP(String htmlContent){
        Document doc = Jsoup.parse(htmlContent);
        Elements paragraphs = doc.select("p");
        for (int i = 2; i < paragraphs.size(); i++) {
            paragraphs.get(i).remove();
        }
        String processedHtmlContent = doc.body().html();
        return processedHtmlContent;
    }



    //回到前台首页
    public static String site_url(){
        return "/back/index";
    }
}
