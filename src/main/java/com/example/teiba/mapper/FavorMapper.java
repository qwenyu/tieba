package com.example.teiba.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.teiba.pojo.Favor;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface FavorMapper extends BaseMapper<Favor> {
    int unread_num(int uid);

    void close_favor(int uid);
}
