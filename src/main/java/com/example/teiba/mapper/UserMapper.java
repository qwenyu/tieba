package com.example.teiba.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.teiba.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface UserMapper extends BaseMapper<User> {
    User queryById(Integer id);
    //查询全部,返回list集合
    List<User> findAlluser();

    Map<String, Object> all_likes(int uid);


}
