package com.example.teiba.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.teiba.pojo.User;
import com.example.teiba.pojo.Visitor;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface VisitorMapper extends BaseMapper<Visitor> {
    int visitor_num(int uid);
}
