package com.example.teiba.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.teiba.pojo.Article;
import com.example.teiba.pojo.message;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper

public interface MessageMapper extends BaseMapper<message> {

    int unmessage_num(int uid);

    void close_message(int uid);






}
