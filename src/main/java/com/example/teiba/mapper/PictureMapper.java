package com.example.teiba.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.teiba.pojo.Picture;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper

public interface PictureMapper extends BaseMapper<Picture> {


}
