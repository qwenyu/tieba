package com.example.teiba.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.teiba.pojo.Commentfavor;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentFavorMapper extends BaseMapper<Commentfavor> {
}
