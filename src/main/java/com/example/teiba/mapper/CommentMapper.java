package com.example.teiba.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.teiba.pojo.Comment;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentMapper extends BaseMapper<Comment> {

//    根据文章id删除评论
    void deleteByArticleId(int id);

    void update_delete(Integer commentId);

    List<Comment> queryDelete();
    List<Comment> queryDeleteLike(String comContent);

    List<Comment> Page(int page_count, int size);
    List<Comment> PageLike(String comContent, int page_count, int size);

    int unread_num(int uid);
//    清空未读消息
    void closeComments(int uid);
}
