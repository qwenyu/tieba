package com.example.teiba.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.teiba.pojo.attention;
import org.springframework.stereotype.Repository;

@Repository
public interface AttentionMapper extends BaseMapper<attention> {
    int unattention_num(int uid);

    void close_attention(int uid);
}
