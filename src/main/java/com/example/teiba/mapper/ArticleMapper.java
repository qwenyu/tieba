package com.example.teiba.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.teiba.pojo.Article;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface ArticleMapper extends BaseMapper<Article> {
    //查询所有文章
    List<Article> findAllarticle();
//插入文章类
    void txtInsert(Article article);
//    根据id查询点赞，浏览量

    Map <String, Integer> likesView(int id);

    //根据文章id删除文章
    void deleteArticle(int id);

    List<Article> findDeleted();

    List<Article> findDeletedLike(String artContent);

    void update_deleted(Integer articleId);

    int deleteLong(Integer articleId);

    Article findId(Integer articleId);




}
