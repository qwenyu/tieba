package com.example.teiba.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.teiba.mapper.CommentMapper;
import com.example.teiba.pojo.Comment;
import com.example.teiba.service.CommentService;

public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements CommentService {
}
