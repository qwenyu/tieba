package com.example.teiba.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.teiba.mapper.UserMapper;
import com.example.teiba.pojo.User;
import com.example.teiba.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    @Autowired
    private  UserMapper usermapper;
    public  String get_username(int uid){
        User user = usermapper.selectById(uid);
        System.out.println("输出了名字");
        return user.getUsername();
    }

}
