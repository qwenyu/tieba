package com.example.teiba.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.teiba.mapper.CommentFavorMapper;
import com.example.teiba.pojo.Commentfavor;
import com.example.teiba.service.CommentFavorService;
import org.springframework.stereotype.Service;

@Service
public class CommentFavorServiceImpl extends ServiceImpl<CommentFavorMapper, Commentfavor> implements CommentFavorService {
}
