package com.example.teiba.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.teiba.pojo.Commentfavor;
import org.springframework.stereotype.Service;


public interface CommentFavorService extends IService<Commentfavor> {
}
