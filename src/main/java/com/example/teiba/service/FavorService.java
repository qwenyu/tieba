package com.example.teiba.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.teiba.pojo.Favor;
import org.springframework.stereotype.Service;

@Service
public interface FavorService extends IService<Favor> {
}
