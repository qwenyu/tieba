package com.example.teiba.service;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.teiba.mapper.UserMapper;
import com.example.teiba.pojo.User;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service

public interface UserService extends IService<User> {
    String get_username(int uid);
}
