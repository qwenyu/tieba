package com.example.teiba.service.Impl;
import com.example.teiba.mapper.ArticleMapper;
import com.example.teiba.mapper.CommentMapper;
import com.example.teiba.mapper.UserMapper;
import com.example.teiba.pojo.User;
import com.example.teiba.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServicelmpl implements ArticleService {

    @Autowired
    private UserMapper userMapper;

    private ArticleMapper articleMapper;
    private CommentMapper commentMapper;
    @Override
    public List<User> findAlluser() {
        return userMapper.findAlluser();
    }

    @Override
    public void deleteArticle(Integer id) {
//        先删除文章相关评论
        commentMapper.deleteByArticleId(id);
//        再删除文章
        articleMapper.deleteById(id);

    }
}
