package com.example.teiba.service;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.teiba.pojo.User;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ArticleService   {
    List<User> findAlluser();

    void deleteArticle(Integer id);
}
