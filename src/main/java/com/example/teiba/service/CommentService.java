package com.example.teiba.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.teiba.pojo.Comment;
import org.springframework.stereotype.Service;

@Service
public interface CommentService extends IService<Comment> {
}
