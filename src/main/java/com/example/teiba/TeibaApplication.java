package com.example.teiba;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.example.teiba.mapper")
public class TeibaApplication {

    public static void main(String[] args) {
        SpringApplication.run(TeibaApplication.class, args);
    }

}
