package com.example.teiba.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor

@Component//注册bean
public class Visitor {
    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer visitorId;

    private Integer userId;

    @TableField(fill = FieldFill.INSERT)
    private Date create_time;

    private Integer readss;
}
