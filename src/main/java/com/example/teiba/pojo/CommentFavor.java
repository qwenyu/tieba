package com.example.teiba.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("commfavor")
public class CommentFavor {
    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer userId;
    private Integer commentId;

    @TableField(fill = FieldFill.INSERT)
    private Date create_time;
    private Integer readss;

    public CommentFavor(Integer userId, Integer commentId) {
        this.userId = userId;
        this.commentId = commentId;
    }

    public CommentFavor(Integer userId, Integer commentId, Integer read) {
        this.userId = userId;
        this.commentId = commentId;
        this.readss = read;
    }
}
