package com.example.teiba.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Favor {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer userId;
    private Integer articleId;
    @TableField(fill = FieldFill.INSERT)
    private Date create_time;

    private Integer readss;

    public Favor(Integer userId, Integer articleId) {
        this.userId = userId;
        this.articleId = articleId;
    }

    public Favor(Integer userId, Integer articleId, Integer read) {
        this.userId = userId;
        this.articleId = articleId;
        this.readss = read;
    }
}
