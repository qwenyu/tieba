package com.example.teiba.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class attention {
    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer userId;

    private Integer beuserId;

    private Integer readss;


    @TableField(fill = FieldFill.INSERT)
    private Date create_time;

    public attention(Integer userId, Integer beuserId, Integer readss) {
        this.userId = userId;
        this.beuserId = beuserId;
        this.readss = readss;
    }
}
