package com.example.teiba.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Comment {
    @TableId(type = IdType.AUTO)
    private Integer commentId;
    @TableField(fill = FieldFill.INSERT)
    private Date create_time;
    private Integer userId;
    private String comContent;
    private Integer articleId;
    private Integer comLikes;
    @TableLogic
    private Integer deleted;

    private int readss;

    public Comment(Integer userId, String comContent, Integer articleId, Integer comLikes) {
        this.userId = userId;
        this.comContent = comContent;
        this.articleId = articleId;
        this.comLikes = comLikes;
    }

    public Comment(Date time, Integer userId, String comContent, Integer articleId, Integer comLikes) {
        this.create_time = time;
        this.userId = userId;
        this.comContent = comContent;
        this.articleId = articleId;
        this.comLikes = comLikes;
    }
}
