package com.example.teiba.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Article {
    @TableId(type = IdType.AUTO)
    private Integer articleId;
    private String artContent;
    @TableLogic
    private Integer deleted;
    private String title;
    @TableField(fill = FieldFill.INSERT)
    private Date create_time;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date update_time;
    private Integer artLikes;
    private Integer views;
    private Integer userId;
    private String tag;





    public Article(String artContent, String title, Integer artLikes, Integer views, Integer userId, String tag) {
        this.artContent = artContent;
        this.title = title;
        this.artLikes = artLikes;
        this.views = views;
        this.userId = userId;
        this.tag = tag;
    }



    public Article(String artContent, Integer deleted, String title, Date create_time, Date update_time, Integer artLikes, Integer views, Integer userId, String tag) {
        this.artContent = artContent;
        this.deleted = deleted;
        this.title = title;
        this.create_time = create_time;
        this.update_time = update_time;
        this.artLikes = artLikes;
        this.views = views;
        this.userId = userId;
        this.tag = tag;
    }
}
