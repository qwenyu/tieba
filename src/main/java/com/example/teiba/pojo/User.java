package com.example.teiba.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@AllArgsConstructor
@NoArgsConstructor

@Component//注册bean

public class User {

    @TableId(type = IdType.AUTO)
    private Integer userId;
    private String username;
    private String password;
    //逻辑删除字段，标记当前记录是否被删除
//    @TableLogic
//    private Integer deleted;
//    @TableField(fill = FieldFill.INSERT)
//    private Date createTime;
//    @TableField(fill = FieldFill.INSERT_UPDATE)
//    private Date updateTime;
    private String phone;
    private String isAdmin;

    private String header;
    @TableLogic
    private Integer deleted;



    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User(String username, String password, String phone, String is_admin) {
        this.username = username;
        this.password = password;
        this.phone = phone;
        this.isAdmin = is_admin;
    }
}
