package com.example.teiba.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class message {
    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer userId;

    private Integer beuserId;

    private String message;



    @TableField(fill = FieldFill.INSERT)
    private Date create_time;

    private Integer readss;
}
