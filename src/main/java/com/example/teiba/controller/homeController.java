package com.example.teiba.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.teiba.mapper.*;
import com.example.teiba.pojo.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class homeController {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private ArticleMapper articleMapper;

    @Autowired
    private CommentMapper commentMapper;

    @Autowired
    private FavorMapper likeMapper;

    @Autowired
    private CommentFavorMapper commentFavorMapper;

    @Autowired
    private AttentionMapper attentionMapper;

    //向前端添加数据
    @GetMapping("/")
    public String home(Model model, @RequestParam(defaultValue = "1") int page_count,HttpSession session, @RequestParam(value = "userId", required = false) Integer id,
                       @RequestParam(value = "num", required = false, defaultValue = "1") int num){
        session.setAttribute("UserIDD", id);

        //获取当前登录的用户信息
        User user = userMapper.selectById(id);

        session.setAttribute("Login_User", user);

        model.addAttribute("Login_User", user);
        if(id != null)
            model.addAttribute("isAdmin", user.getIsAdmin());

        //获取排名数据
        QueryWrapper<Article> articleQueryWrapper1 = new QueryWrapper<>();
        QueryWrapper<Article> artLikes = articleQueryWrapper1.orderByDesc("artLikes");
        List<Article> articles = articleMapper.selectList(artLikes);
        ArrayList<Article> articles1 = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            articles1.add(articles.get(i));
        }

        model.addAttribute("rank", articles1);

        int sizess = articleMapper.selectList(null).size();
        int total_page;
        if(sizess % 5 == 0){
            total_page = sizess / 5;
        }else{
            total_page = sizess / 5 + 1;
        }

        QueryWrapper<Article> articleQueryWrapper = new QueryWrapper<>();
        articleQueryWrapper.orderByDesc("create_time");

        IPage<Article> page = new Page<>(page_count, 5);

        IPage<Article> selectPage = articleMapper.selectPage(page, articleQueryWrapper);

        List<Article> records = selectPage.getRecords();

        HashMap<Integer, Boolean> mapP1 = new HashMap<>();
        for (Article article :records) {
            System.out.println("article = " + article);
            mapP1.put(article.getArticleId(), false);
        }

        if(id != null){
            for (Article article :records) {
                int flag = 0;
                QueryWrapper<Favor> likeQueryWrapper = new QueryWrapper<>();

                likeQueryWrapper.eq("articleId", article.getArticleId());
                List<Favor> likes = likeMapper.selectList(likeQueryWrapper);
                for (Favor like :likes) {
                    if(like.getUserId().equals(id)){
                        flag = 1;
                    }
                }
                if(flag == 1){
                    mapP1.put(article.getArticleId(), true);
                }else{
                    mapP1.put(article.getArticleId(), false);
                }
            }

        }

        model.addAttribute("mapP1", mapP1);

        model.addAttribute("allArticles", records);
        HashMap<Integer, String>  map0 = new HashMap<>();//存头像
        Map<Integer, Integer> map = new HashMap<>();//存发表的文章数量
        HashMap<Integer, String> map1 = new HashMap<>();//存文章作者名字

        for (Article article :records) {
            User userId = userMapper.selectOne(new QueryWrapper<User>()
                    .eq("userId", article.getUserId()));
            List<Comment> articleId = commentMapper.selectList(new QueryWrapper<Comment>().eq("articleId", article.getArticleId()));
            map0.put(userId.getUserId(), userId.getHeader());
            map.put(article.getArticleId(), articleId.size());
            map1.put(article.getArticleId(), userId.getUsername());
        }
        model.addAttribute("total_page", total_page);
        model.addAttribute("page_count", page_count);
        model.addAttribute("map0", map0);
        model.addAttribute("map", map);
        model.addAttribute("map1", map1);
        model.addAttribute("userIDD", id);
        model.addAttribute("num", num);
        return "home/home";
    }

    @GetMapping("/home/search")
    public String search(@RequestParam(value = "keyboard", required = false, defaultValue = "") String keyboard, Model model, HttpSession session,
                         @RequestParam(value = "num", required = false, defaultValue = "1") int num){
        Integer userIDD = (Integer) session.getAttribute("UserIDD");
        System.out.println("userIDD = " + userIDD);

        if(!"".equals(keyboard))
            session.setAttribute("keyboard", keyboard);
        else{
            keyboard = (String) session.getAttribute("keyboard");
        }
        //获取当前登录的用户信息
        User user00 = userMapper.selectById(userIDD);
        model.addAttribute("Login_User", user00);
        model.addAttribute("userIDD", userIDD);

        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.like("username", keyboard);
        User user = userMapper.selectOne(userQueryWrapper);
        HashMap<Integer, String> map2 = new HashMap<>();
        HashMap<Integer, String>  map00 = new HashMap<>();//存头像
        HashMap<Integer, String> map1 = new HashMap<>();//存文章作者名字

        List<Article> articles = new ArrayList<>();
        HashMap<Integer, Boolean> mapP1 = new HashMap<>();

        if(user != null){
            QueryWrapper<Article> articleQueryWrapper = new QueryWrapper<>();
            articleQueryWrapper.eq("userId", user.getUserId()).orderByDesc("create_time");
            articles = articleMapper.selectList(articleQueryWrapper);

            for (Article article :articles) {
                User userId = userMapper.selectOne(new QueryWrapper<User>().eq("userId", article.getUserId()));
                map2.put(article.getArticleId(), userId.getUsername());
                map00.put(userId.getUserId(), userId.getHeader());
                mapP1.put(article.getArticleId(), false);
                map1.put(article.getArticleId(), userId.getUsername());
            }

            model.addAttribute("map00", map00);
            model.addAttribute("map2", map2);
            model.addAttribute("information", articles);
            model.addAttribute("num", num);
        }else{
            QueryWrapper<Article> articleQueryWrapper2 = new QueryWrapper<>();
            articleQueryWrapper2.like("title", keyboard).orderByDesc("create_time");
            articles = articleMapper.selectList(articleQueryWrapper2);
            for (Article article :articles) {
                User userId = userMapper.selectOne(new QueryWrapper<User>().eq("userId", article.getUserId()));
                map2.put(article.getArticleId(), userId.getUsername());
                map00.put(userId.getUserId(), userId.getHeader());
                mapP1.put(article.getArticleId(), false);
                map1.put(article.getArticleId(), userId.getUsername());
            }
            model.addAttribute("map2", map2);
            model.addAttribute("map00", map00);
            model.addAttribute("information", articles);
            model.addAttribute("num", num);
        }


        if(userIDD != null){
            for (Article article :articles) {
                int flag = 0;
                QueryWrapper<Favor> likeQueryWrapper = new QueryWrapper<>();

                likeQueryWrapper.eq("articleId", article.getArticleId());
                List<Favor> likes = likeMapper.selectList(likeQueryWrapper);
                for (Favor like :likes) {
                    if(like.getUserId().equals(userIDD)){
                        flag = 1;
                    }
                }
                if(flag == 1){
                    mapP1.put(article.getArticleId(), true);
                }else{
                    mapP1.put(article.getArticleId(), false);
                }
            }

        }
        model.addAttribute("mapP1", mapP1);
        model.addAttribute("map1", map1);



        return "/home/search";
    }

    @GetMapping("/home/boke")
    public String boke(Integer id, Model model, HttpSession session, HttpServletRequest request,
                       @RequestParam(value = "num", required = false, defaultValue = "1") int num){
        int likesss = 0;
        Integer userIDD = (Integer) session.getAttribute("UserIDD");
        User userrr = (User) session.getAttribute("Login_User");
        model.addAttribute("uuiui", userrr);

        System.out.println("userIDD = " + userIDD);

        //获取当前登录的用户信息
        User user00 = userMapper.selectById(userIDD);
        model.addAttribute("Login_User", user00);

        HashMap<Integer, String> map3 = new HashMap<>();
        HashMap<Integer, String> map4 = new HashMap<>();//存放评论人头像
        HashMap<Integer, String> map5 = new HashMap<>();//存放评论用户名
        HashMap<String, Integer> map6 = new HashMap<>();//存放博主发布博客数
        HashMap<String, Integer> map7 = new HashMap<>();//存放博主所有获赞数


        QueryWrapper<Comment> commentQueryWrapper = new QueryWrapper<>();
        commentQueryWrapper.eq("articleId", id).orderByDesc("comLikes");

        Article article = articleMapper.selectById(id);
        User user = userMapper.selectById(article.getUserId());

        QueryWrapper<attention> attentionQueryWrapper = new QueryWrapper<>();
        attentionQueryWrapper.eq("beUserId", user.getUserId());
        List<attention> attentions1 = attentionMapper.selectList(attentionQueryWrapper);
        model.addAttribute("count_size", attentions1.size());


        List<Article> userId = articleMapper.selectList(new QueryWrapper<Article>().eq("userId", user.getUserId()));
        map6.put(user.getUsername(), userId.size());
        for (Article article1 :userId) {
            likesss += article1.getArtLikes();
        }

        List<Comment> userId1 = commentMapper.selectList(new QueryWrapper<Comment>().eq("userId", user.getUserId()));
        for (Comment comment :userId1) {
            likesss += comment.getComLikes();
        }
        map7.put(user.getUsername(), likesss);


        HashMap<Integer, Boolean> mapP2 = new HashMap<>();

        List<Comment> comments = commentMapper.selectList(commentQueryWrapper);
        for (Comment comment :comments) {
            User user1 = userMapper.selectById(comment.getUserId());
            map4.put(comment.getUserId(), user1.getHeader());
            map5.put(comment.getUserId(), user1.getUsername());
            mapP2.put(comment.getCommentId(), false);
        }
        Comment comment = new Comment();
        map3.put(article.getArticleId(), user.getUsername());

        HashMap<Integer, Boolean> mapP1 = new HashMap<>();
        mapP1.put(article.getArticleId(), false);

        model.addAttribute("attention", false);

        if(userIDD != null){
            for (Comment comment1 :comments) {
                int flag = 0;
                QueryWrapper<Commentfavor> likeQueryWrapper = new QueryWrapper<>();

                likeQueryWrapper.eq("commentId", comment1.getCommentId());
                List<Commentfavor> likes = commentFavorMapper.selectList(likeQueryWrapper);
                for (Commentfavor like :likes) {
                    if(like.getUserId().equals(userIDD)){
                        flag = 1;
                    }
                }
                if(flag == 1){
                    mapP2.put(comment1.getCommentId(), true);
                }else{
                    mapP2.put(comment1.getCommentId(), false);
                }
            }

            QueryWrapper<Favor> favorQueryWrapper = new QueryWrapper<>();
            favorQueryWrapper.eq("articleId", article.getArticleId());
            List<Favor> favors = likeMapper.selectList(favorQueryWrapper);
            int flag1 = 0;
            for (Favor favor :favors) {
                if(favor.getUserId().equals(userIDD)){
                    flag1 = 1;
                }
            }
            if(flag1 == 1){
                mapP1.put(article.getArticleId(), true);
            }else{
                mapP1.put(article.getArticleId(), false);
            }

            QueryWrapper<attention> attentionMapperQueryWrapper = new QueryWrapper<>();
            attentionMapperQueryWrapper.eq("userId", userIDD);

            int flag = 0;
            List<attention> attentions = attentionMapper.selectList(attentionMapperQueryWrapper);
            for (attention attention :attentions) {
                if(attention.getBeuserId().equals(user.getUserId())){
                    flag = 1;
                }
            }
            if(flag == 1){
                model.addAttribute("attention", true);
            }else{
                model.addAttribute("attention", false);
            }
        }

        model.addAttribute("mapP1", mapP1);
        model.addAttribute("mapP2", mapP2);
        model.addAttribute("boke_article", article);
        model.addAttribute("map3", map3);
        model.addAttribute("map4", map4);
        model.addAttribute("map5", map5);
        model.addAttribute("map6", map6);
        model.addAttribute("map7", map7);
        model.addAttribute("boke_user", user);
        model.addAttribute("userIDD", userIDD);
        model.addAttribute("commentList", comments);
        model.addAttribute("num", num);
        return "/home/boke";
    }

    @GetMapping("/home/insert_comment")
    public String insert_comment(String message,@RequestParam("userId") Integer userId,@RequestParam("articleId") Integer articleId, Model model){
        if(userId != null){
            commentMapper.insert(new Comment(userId, message, articleId, 0));
            return "redirect:/home/boke?id=" + articleId;
        }else{
            return "/index";
        }
    }

    @GetMapping("/home/like")
    public String like(@RequestParam("articleId") Integer id, HttpSession session,
                       @RequestParam(value = "html", required = false, defaultValue = "") String html){
        Integer userIDD = (Integer) session.getAttribute("UserIDD");
        String keyboard = (String) session.getAttribute("keyboard");
        int like_sum = 0;
        int flag = 0;
        if(userIDD != null){
            QueryWrapper<Favor> likeQueryWrapper = new QueryWrapper<>();
            likeQueryWrapper.eq("articleId", id);
            List<Favor> likes = likeMapper.selectList(likeQueryWrapper);
            for (Favor like :likes) {
                if(like.getUserId().equals(userIDD)){
                    flag = 1;
                }
            }
            Article article = articleMapper.selectById(id);
            if(flag == 0){
                like_sum = article.getArtLikes() + 1;
                article.setArtLikes(like_sum);
                articleMapper.updateById(article);
                likeMapper.insert(new Favor(userIDD, article.getArticleId()));
            }else{
                like_sum = article.getArtLikes() - 1;
                article.setArtLikes(like_sum);
                articleMapper.updateById(article);
                likeMapper.delete(new QueryWrapper<Favor>().eq("userId", userIDD));
            }
            if("home".equals(html)){
                return "redirect:/?userId=" + userIDD;
            }else{
                return "redirect:/home/search";
            }
        }else{
            return "redirect:/index";
        }
    }

    @GetMapping("/home/like111")
    public String like111(@RequestParam("articleId") Integer id, HttpSession session){
        Integer userIDD = (Integer) session.getAttribute("UserIDD");
        int like_sum = 0;
        int flag = 0;
        if(userIDD != null){
            QueryWrapper<Favor> likeQueryWrapper = new QueryWrapper<>();
            likeQueryWrapper.eq("articleId", id);
            List<Favor> likes = likeMapper.selectList(likeQueryWrapper);
            for (Favor like :likes) {
                if(like.getUserId().equals(userIDD)){
                    flag = 1;
                }
            }
            Article article = articleMapper.selectById(id);
            if(flag == 0){
                like_sum = article.getArtLikes() + 1;
                article.setArtLikes(like_sum);
                articleMapper.updateById(article);
                likeMapper.insert(new Favor(userIDD, article.getArticleId()));
            }else{
                like_sum = article.getArtLikes() - 1;
                article.setArtLikes(like_sum);
                articleMapper.updateById(article);
                likeMapper.delete(new QueryWrapper<Favor>().eq("userId", userIDD));
            }
            return "redirect:/home/boke?id=" + id;

        }else{
            return "redirect:/index";
        }
    }

    @GetMapping("/home/comment_like")
    public String comment_like(HttpSession session, @RequestParam("commentId") Integer id){
        Integer userIDD = (Integer) session.getAttribute("UserIDD");
        int like_sum = 0;
        int flag = 0;
        if(userIDD != null){
            QueryWrapper<Commentfavor> likeQueryWrapper = new QueryWrapper<>();
            likeQueryWrapper.eq("commentId", id);
            List<Commentfavor> likes = commentFavorMapper.selectList(likeQueryWrapper);
            for (Commentfavor like :likes) {
                if(like.getUserId().equals(userIDD)){
                    flag = 1;
                }
            }
            Comment comment = commentMapper.selectById(id);

            if(flag == 0){
                like_sum = comment.getComLikes() + 1;
                comment.setComLikes(like_sum);
                commentMapper.updateById(comment);
                commentFavorMapper.insert(new Commentfavor(userIDD, id));
            }else{
                like_sum = comment.getComLikes() - 1;
                comment.setComLikes(like_sum);
                commentMapper.updateById(comment);
                commentFavorMapper.delete(new QueryWrapper<Commentfavor>().eq("userId", userIDD));
            }
            return "redirect:/home/boke?id=" + comment.getArticleId();
        }else{
            return "redirect:/index";
        }
    }

    @GetMapping("/home/attention")
    public String attention(Integer Id, HttpSession session, Model model, Integer articleId){
        Integer userIDD = (Integer) session.getAttribute("UserIDD");
        if(userIDD != null){
            int flag = 0;
            QueryWrapper<attention> attentionQueryWrapper = new QueryWrapper<>();
            attentionQueryWrapper.eq("userId", userIDD);
            List<attention> attentions = attentionMapper.selectList(attentionQueryWrapper);
            for (attention attention :attentions) {
                if(attention.getBeuserId().equals(Id)){
                    flag = 1;
                }
            }

            if(flag == 1){
                QueryWrapper<attention> attentionQueryWrapper1 = new QueryWrapper<>();
                attentionQueryWrapper1.eq("userId", userIDD).eq("beUserId", Id);
                attentionMapper.delete(attentionQueryWrapper1);
            }else{
                attentionMapper.insert(new attention(userIDD, Id, 0));
            }

            return "redirect:/home/boke?id=" + articleId;
        }
        return "redirect:/index";
    }
}
