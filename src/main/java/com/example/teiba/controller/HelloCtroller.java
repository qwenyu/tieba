package com.example.teiba.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.example.teiba.mapper.UserMapper;
import com.example.teiba.pojo.User;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
public class HelloCtroller {
    @Autowired
    private UserMapper userMapper;

    @GetMapping("/index")
    public String index(Model model, HttpServletRequest request){
        String msg = (String) request.getServletContext().getAttribute("msg");
        model.addAttribute("msg", msg);
        return "login_register/index";
    }

    @GetMapping ("/zhuce")
    public String zhuce(){
        return "login_register/zhuce";
    }

    @GetMapping("/zhuce1")
    public String zhuce1(String phone, @RequestParam("yzm") String yzm, HttpSession session, Model model){
        session.setAttribute("zhuce_phone", phone);
        String attribute = (String) session.getAttribute("codeImg");
        System.out.println("yzm=" + yzm + "phone=" + phone);
        //判断验证码是否正确
        if(attribute.equals(yzm)){
            return "login_register/zhuce1";
        }else{
            model.addAttribute("msg", "验证码错误");
            return "login_register/zhuce";
        }
    }

    //注册成功
    @GetMapping("/zhuceSucc")
    public String zhuceSucc(@RequestParam("infor_yzm") String infor_yzm, HttpSession session, Model model,String password, String username, String password1){
        String attribute = (String) session.getAttribute("codeImg");
        if(password1.equals(password1)){
            if(attribute.equals(infor_yzm)){
                userMapper.insert(new User(username, password, (String) session.getAttribute("zhuce_phone"), "0"));
                return "login_register/zhuceSucc";
            }else{
                model.addAttribute("msg", "验证码不一致");
                return "login_register/zhuce1";
            }
        }else{
            model.addAttribute("msg", "密码不一致");
            return "login_register/zhuce1";
        }


    }

    @GetMapping("/forgetPwd")
    public String password(){
        return "login_register/password";
    }

    //登录验证
    @GetMapping("/login")
    public String login(String username, String password, RedirectAttributes redirectAttributes, HttpSession session, Model model, String verifyCode){
        System.out.println("verifyCode:" + verifyCode);
        String attribute = (String) session.getAttribute("codeImg");

        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.eq("username", username)
                .eq("password", password);

        User user = userMapper.selectOne(userQueryWrapper);

        if(user != null){
            session.setAttribute("LLogin", user);
            if(verifyCode.equals(attribute)){
                return "/home/UUUser";
            }else{
                model.addAttribute("msg", "验证码错误");
                return "login_register/index";
            }
        }else if(user == null){
            QueryWrapper<User> userQueryWrapper2 = new QueryWrapper<>();
            userQueryWrapper.eq("phone", username)
                    .eq("password", password);
            User user2 = userMapper.selectOne(userQueryWrapper);
            session.setAttribute("LLogin", user2);
            redirectAttributes.addAttribute("LLogin", user2);
            if(verifyCode.equals(attribute)){
                return "/home/UUUser";
            }else{
                model.addAttribute("msg", "验证码错误");
                return "login_register/index";
            }
        } else {
            model.addAttribute("msg", "密码或用户名错误");
            return "login_register/index";
        }
    }


    @GetMapping("/second")
    public String second(@RequestParam("phone") String phone, Model model){
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.eq("phone", phone);

        User user = userMapper.selectOne(userQueryWrapper);

        System.out.println("user.getUser_id() = " + user.getUserId());

        System.out.println("user = " + user);
        model.addAttribute("usernamesss", user);
        return "login_register/password_2";
    }

    @GetMapping("/three")
    public String three(@RequestParam("id") String id, String password, String password1, String yyzzmm, Model model, HttpSession session){
        System.out.println("id = " + id);
        Integer ID = Integer.parseInt(id);
        String attribute = (String) session.getAttribute("codeImg");
        if(password1.equals(password)){
            if(yyzzmm.equals(attribute)){
                QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
                User user = userMapper.selectOne(userQueryWrapper.eq("userId", ID));
                user.setPassword(password);
                userMapper.updateById(user);
                return "login_register/password_3";
            }else {
                model.addAttribute("msg", "验证码不正确");
                return "login_register/password_2";
            }
        }else{
            model.addAttribute("msg", "密码不一致");
            return "login_register/password_2";
        }

    }


}

