package com.example.teiba.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.teiba.mapper.*;
import com.example.teiba.pojo.Article;
import com.example.teiba.pojo.Comment;
import com.example.teiba.pojo.Favor;
import com.example.teiba.pojo.User;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class BackEndController {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private ArticleMapper articleMapper;

    @Autowired
    private CommentMapper commentMapper;

    @Autowired
    private FavorMapper likeMapper;

    @Autowired
    private CommentFavorMapper commentFavorMapper;

    @GetMapping("/backend/index")
    public String index(@RequestParam(value = "id", required = false) int id, Model model,
                        HttpSession session,
                        @RequestParam(value = "num", required = false) int num, HttpServletRequest request){
        session.setAttribute("userId", id);
        model.addAttribute("userIDD", id);
        model.addAttribute("num", num);

        boolean isFirstVisit = true;

        if(num == 1){
            isFirstVisit =false;
            num++;
        }
        session.setAttribute("isFirstVisit", isFirstVisit);
        session.setAttribute("num", num);


        model.addAttribute("isFirstVisit", isFirstVisit);
        User user = userMapper.selectById(id);
        String isAdmin = user.getIsAdmin();
        if("1".equals(isAdmin)){
            List<Comment> comments = commentMapper.selectList(null);
            model.addAttribute("commentNum", comments.size());
            List<Article> articles = articleMapper.selectList(null);
            model.addAttribute("articleNum", articles.size());
            List<User> users = userMapper.selectList(null);
            model.addAttribute("userNum", users.size());

            model.addAttribute("articleList", articles);
            return "backend/index";
        }else{
            String message = "您不是管理员，没有此权限";
            request.getServletContext().setAttribute("msg", message);
            return "redirect:/index";
        }

    }


    @GetMapping("/backend/controlpanel")
    public String control(Model model, HttpSession session, @RequestParam(value = "page_count", defaultValue = "1") int page_count,
                          @RequestParam(value = "articleId", required = false, defaultValue = "-1") Integer articleId){

        Integer id = (Integer) session.getAttribute("userId");
        Integer num = (Integer) session.getAttribute("num");

        boolean attribute = (boolean) session.getAttribute("isFirstVisit");
        if(articleId != -1){
            articleMapper.deleteById(articleId);
        }

        List<Comment> comments = commentMapper.selectList(null);
        model.addAttribute("commentNum", comments.size());
        List<Article> articles = articleMapper.selectList(new QueryWrapper<Article>().orderByDesc("create_time"));
        model.addAttribute("articleNum", articles.size());
        List<User> users = userMapper.selectList(null);
        model.addAttribute("userNum", users.size());

        model.addAttribute("UUID", id);
        model.addAttribute("isFirstVisit", attribute);
        model.addAttribute("num", num);

        int sizess = articleMapper.selectList(null).size();
        int total_page;
        if(sizess % 8 == 0){
            total_page = sizess / 6;
        }else{
            total_page = sizess / 6 + 1;
        }
        QueryWrapper<Article> articleQueryWrapper = new QueryWrapper<>();
        articleQueryWrapper.orderByDesc("create_time");

        IPage<Article> page = new Page<>(page_count, 6);
        IPage<Article> selectPage = articleMapper.selectPage(page, articleQueryWrapper);

        List<Article> records = selectPage.getRecords();
        model.addAttribute("articleList", records);
        model.addAttribute("total_page", total_page);
        model.addAttribute("page_count", page_count);

        return "backend/controlpanel";
    }

//    @GetMapping("/backend/delete")
//    public String delete(Integer articleId, Model model, HttpSession session){
//        Integer id = (Integer) session.getAttribute("userId");
//        Integer num = (Integer) session.getAttribute("num");
//
//        articleMapper.deleteById(articleId);
//
//        List<Comment> comments = commentMapper.selectList(null);
//        model.addAttribute("commentNum", comments.size());
//        List<Article> articles = articleMapper.selectList(new QueryWrapper<Article>().orderByDesc("create_time"));
//        model.addAttribute("articleNum", articles.size());
//        List<User> users = userMapper.selectList(null);
//        model.addAttribute("userNum", users.size());
//
//        model.addAttribute("articleList", articles);
//
//        return "backend/index";
//    }

    @GetMapping("/backend/list")
    public String list(Model model, HttpSession session, @RequestParam(value = "page_count", defaultValue = "1") int page_count,
                       @RequestParam(value = "xinxi", required = false, defaultValue = "") String xinxi,
                       @RequestParam(value = "articleId", required = false, defaultValue = "-1") Integer articleId){
        Integer id = (Integer) session.getAttribute("userId");
        Integer num = (Integer) session.getAttribute("num");

        if(articleId != -1){
            articleMapper.deleteById(articleId);
        }

        List<Article> articles = new ArrayList<>();
        int sizess;
        QueryWrapper<Article> articleQueryWrapper = new QueryWrapper<>();


        if("".equals(xinxi)){
            articles = articleMapper.selectList(new QueryWrapper<Article>().orderByDesc("create_time"));
            sizess = articleMapper.selectList(null).size();
            articleQueryWrapper.orderByDesc("create_time");
        }else{
            articles = articleMapper.selectList(new QueryWrapper<Article>().orderByDesc("create_time").like("artContent", xinxi));
            sizess = articleMapper.selectList(new QueryWrapper<Article>().like("artContent", xinxi)).size();
            articleQueryWrapper.orderByDesc("create_time").like("artContent", xinxi);
        }

        model.addAttribute("UUID", id);
        model.addAttribute("num", num);

        HashMap<Integer, String> map0 = new HashMap<>();
        for (Article article :articles) {
            User user = userMapper.selectById(article.getUserId());
            map0.put(article.getArticleId(), user.getUsername());
        }
        model.addAttribute("map0", map0);


        int total_page;
        if(sizess % 8 == 0){
            total_page = sizess / 8;
        }else{
            total_page = sizess / 8 + 1;
        }


        IPage<Article> page = new Page<>(page_count, 6);
        IPage<Article> selectPage = articleMapper.selectPage(page, articleQueryWrapper);

        List<Article> records = selectPage.getRecords();
        model.addAttribute("articleList", records);
        model.addAttribute("total_page", total_page);
        model.addAttribute("page_count", page_count);

        return "backend/article";
    }

    @GetMapping("/backend/trash")
    public String trash(Model model, HttpSession session, @RequestParam(value = "page_count", defaultValue = "1") int page_count,
                        @RequestParam(value = "aaarrrid", required = false, defaultValue = "-1") Integer arrid,
                        @RequestParam(value = "xinxi", required = false, defaultValue = "") String xinxi){
        Integer id = (Integer) session.getAttribute("userId");
        Integer num = (Integer) session.getAttribute("num");

        if(arrid != -1){
            articleMapper.update_deleted(arrid);
        }

        List<Article> articles = new ArrayList<>();

        if("".equals(xinxi)){
            articles = articleMapper.findDeleted();
        }else{
            articles = articleMapper.findDeletedLike(xinxi);
        }


        HashMap<Integer, String> map1 = new HashMap<>();

        for (Article article :articles) {
            User user = userMapper.selectById(article.getUserId());
            map1.put(article.getArticleId(), user.getUsername());
        }


        for (Article article :articles) {
            System.out.println(article.getArticleId() + map1.get(article.getArticleId()));
        }


        int sizess = articles.size();
        int total_page;
        if(sizess % 8 == 0){
            total_page = sizess / 8;
        }else{
            total_page = sizess / 8 + 1;
        }
        QueryWrapper<Article> articleQueryWrapper = new QueryWrapper<>();
        articleQueryWrapper.orderByDesc("create_time");

        IPage<Article> page = new Page<>(page_count, 7);
        IPage<Article> selectPage = articleMapper.selectPage(page, articleQueryWrapper);

        List<Article> records = selectPage.getRecords();
        model.addAttribute("articleList", articles);
        model.addAttribute("total_page", total_page);
        model.addAttribute("page_count", page_count);

        model.addAttribute("map1", map1);

        model.addAttribute("UUID", id);
        model.addAttribute("num", num);
        return "/backend/article_recycle";
    }

    @GetMapping("/backend/comment")
    public String comment(Model model, HttpSession session, @RequestParam(value = "page_count", defaultValue = "1") int page_count,
                          @RequestParam(value = "deletedId" ,required = false, defaultValue = "-1") Integer deletedId,
                          @RequestParam(value = "xinxi", required = false, defaultValue = "") String xinxi){
        Integer id = (Integer) session.getAttribute("userId");
        Integer num = (Integer) session.getAttribute("num");

        model.addAttribute("UUID", id);
        model.addAttribute("num", num);

        if(deletedId != -1){
            commentMapper.deleteById(deletedId);
        }

        List<Comment> comments = new ArrayList<>();
        QueryWrapper<Comment> commentQueryWrapper = new QueryWrapper<>();
        if("".equals(xinxi)){
            comments = commentMapper.selectList(null);
            commentQueryWrapper.orderByDesc("create_time");
        }else{
            comments = commentMapper.selectList(new QueryWrapper<Comment>().like("comContent", xinxi));
            commentQueryWrapper.orderByDesc("create_time").like("comContent", xinxi);
        }

        int size = comments.size();
        int total_page;
        if(size % 8 == 0){
            total_page = size / 8;
        }else{
            total_page = size / 8 + 1;
        }

        IPage<Comment> page = new Page<>(page_count, 8);
        IPage<Comment> selectPage = commentMapper.selectPage(page, commentQueryWrapper);

        HashMap<Integer, String> map2 = new HashMap<>();
        HashMap<Integer, String> map3 = new HashMap<>();
        List<Comment> records = selectPage.getRecords();
        for (Comment comment :records) {
            User user = userMapper.selectById(comment.getUserId());
            map2.put(comment.getCommentId(), user.getUsername());

            Article article = articleMapper.findId(comment.getArticleId());
            map3.put(comment.getCommentId(), article.getTitle());
        }


        model.addAttribute("commentList", records);
        model.addAttribute("total_page", total_page);
        model.addAttribute("page_count", page_count);
        model.addAttribute("map2", map2);
        model.addAttribute("map3", map3);
        return "backend/column";
    }

    @GetMapping("/backend/comment_trash")
    public String comment_trash(Model model, HttpSession session, @RequestParam(value = "page_count", defaultValue = "1") int page_count,
                                @RequestParam(value = "CommId", defaultValue = "-1") Integer CommId,
                                @RequestParam(value = "xinxi", required = false, defaultValue = "") String xinxi){
        Integer id = (Integer) session.getAttribute("userId");
        Integer num = (Integer) session.getAttribute("num");

        model.addAttribute("UUID", id);
        model.addAttribute("num", num);

        if(CommId != -1){
            commentMapper.update_delete(CommId);
        }

        List<Comment> comments = new ArrayList<>();
        List<Comment> records = new ArrayList<>();
        if("".equals(xinxi)){
            comments = commentMapper.queryDelete();
        }else{
            comments = commentMapper.queryDeleteLike(xinxi);
        }


        int size = comments.size();
        int total_page;
        if(size % 8 == 0){
            total_page = size / 8;
        }else{
            total_page = size / 8 + 1;
        }
//        QueryWrapper<Comment> commentQueryWrapper = new QueryWrapper<>();
//        commentQueryWrapper.orderByDesc("create_time");
//        IPage<Comment> page = new Page<>(page_count, 8);
//        IPage<Comment> selectPage = commentMapper.selectPage(page, commentQueryWrapper);

        HashMap<Integer, String> map2 = new HashMap<>();
        HashMap<Integer, String> map3 = new HashMap<>();
//        List<Comment> records = selectPage.getRecords();
        for (Comment comment :comments) {
            User user = userMapper.selectById(comment.getUserId());
            map2.put(comment.getCommentId(), user.getUsername());

            Article article = articleMapper.findId(comment.getArticleId());
            map3.put(comment.getCommentId(), article.getTitle());
        }


        model.addAttribute("commentList", comments);
        model.addAttribute("total_page", total_page);
        model.addAttribute("page_count", page_count);
        model.addAttribute("map2", map2);
        model.addAttribute("map3", map3);
        return "backend/column_recycle";
    }

    @GetMapping("/backend/admin")
    public String admin(Model model, HttpSession session, @RequestParam(value = "page_count", defaultValue = "1") int page_count,
                        @RequestParam(value = "xinxi", required = false, defaultValue = "") String xinxi,
                        @RequestParam(value = "deleteAdminId", required = false, defaultValue = "-1") Integer deleteAdminId){
        Integer id = (Integer) session.getAttribute("userId");
        Integer num = (Integer) session.getAttribute("num");

        model.addAttribute("UUID", id);
        model.addAttribute("num", num);

        if(deleteAdminId != -1){
            userMapper.deleteById(deleteAdminId);
        }

        List<User> isAdmin = new ArrayList<>();
        QueryWrapper<User> isAdmin1 = new QueryWrapper<User>();
        if("".equals(xinxi)){
            isAdmin = userMapper.selectList(new QueryWrapper<User>().eq("isAdmin", "1"));
            isAdmin1.eq("isAdmin", "1");
        }else{
            isAdmin = userMapper.selectList(new QueryWrapper<User>().eq("isAdmin", "1").like("username", xinxi));
            isAdmin1.eq("isAdmin", "1").like("username", xinxi);
        }


//        model.addAttribute("admins", isAdmin);
        int size = isAdmin.size();
        int total_page;
        if(size % 8 == 0){
            total_page = size / 8;
        }else{
            total_page = size / 8 + 1;
        }


        IPage<User> page = new Page<>(page_count, 8);
        IPage<User> selectPage = userMapper.selectPage(page, isAdmin1);
        List<User> records = selectPage.getRecords();

        model.addAttribute("admins", records);
        model.addAttribute("total_page", total_page);
        model.addAttribute("page_count", page_count);

        return "backend/administrators";
    }

    @PostMapping("/backend/addAdmin")
    public String addAdmin(Model model, String username, String password, String phone){
        userMapper.insert(new User(username, password, phone, "1"));
        return "redirect:/backend/admin";
    }

    @GetMapping("/backend/user")
    public String user(Model model, HttpSession session, @RequestParam(value = "page_count", defaultValue = "1") int page_count,
                       @RequestParam(value = "upgrade", required = false, defaultValue = "-1") Integer upgrade,
                       @RequestParam(value = "deleteId", required = false, defaultValue = "-1") Integer deleteId,
                       @RequestParam(value = "xinxi", required = false, defaultValue = "") String xinxi){
        Integer id = (Integer) session.getAttribute("userId");
        Integer num = (Integer) session.getAttribute("num");

        model.addAttribute("UUID", id);
        model.addAttribute("num", num);

        if(upgrade != -1){
            QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
            User user = userMapper.selectOne(userQueryWrapper.eq("userId", upgrade));
            user.setIsAdmin("1");
            userMapper.updateById(user);
        }

        if(deleteId != -1){
            userMapper.deleteById(deleteId);
        }

        List<User> isUser = new ArrayList<>();
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();

        if("".equals(xinxi)){
            isUser = userMapper.selectList(new QueryWrapper<User>().eq("isAdmin", "0"));
            userQueryWrapper.eq("isAdmin", "0");
        }else{
            isUser = userMapper.selectList(new QueryWrapper<User>().eq("isAdmin", "0").like("username", xinxi));
            userQueryWrapper.eq("isAdmin", "0").like("username", xinxi);
        }


        int size = isUser.size();
        int total_page;
        if(size % 8 == 0){
            total_page = size / 8;
        }else{
            total_page = size / 8 + 1;
        }
        IPage<User> page = new Page<>(page_count, 8);
        IPage<User> selectPage = userMapper.selectPage(page, userQueryWrapper);
        List<User> records = selectPage.getRecords();

        int likesss = 0;
        HashMap<Integer, Integer> map5 = new HashMap<>();//博客数
        HashMap<Integer, Integer> map6 = new HashMap<>();//收到点赞数
        for (User user :records) {
            List<Article> userId = articleMapper.selectList(new QueryWrapper<Article>().eq("userId", user.getUserId()));
            for (Article article1 :userId) {
                likesss += article1.getArtLikes();
            }

            List<Comment> userId1 = commentMapper.selectList(new QueryWrapper<Comment>().eq("userId", user.getUserId()));
            for (Comment comment :userId1) {
                likesss += comment.getComLikes();
            }
            map5.put(user.getUserId(), userId.size());
            map6.put(user.getUserId(), likesss);
        }


        model.addAttribute("users", records);
        model.addAttribute("total_page", total_page);
        model.addAttribute("page_count", page_count);
        model.addAttribute("map5", map5);
        model.addAttribute("map6", map6);

        return "backend/user";

    }

    @PostMapping("/backend/addUser")
    public String addUser(Model model, String username, String password, String phone){
        userMapper.insert(new User(username, password, phone, "0"));
        return "redirect:/backend/user";
    }

    @GetMapping("/backend/friend")
    public String friend(){
        return "/backend/friendlinks";
    }

}
