package com.example.teiba.controller;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.teiba.entity.ResponseData;
import com.example.teiba.mapper.*;
import com.example.teiba.mapper.PictureMapper;
import com.example.teiba.pojo.*;
import jakarta.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
@Controller
public class backController {


    @Autowired
    private UserMapper usermapper;
    @Autowired
    private ArticleMapper articleMapper;
    @Autowired

    private AttentionMapper attentionMapper;
    @Autowired

    private CommentMapper commentMapper;
    @Autowired

    private FavorMapper favorMapper;
    @Autowired
    private MessageMapper messageMapper;

    @Autowired
    private VisitorMapper visitorMapper;

    @Autowired
    private PictureMapper pictureMapper;


    @GetMapping("/back")
    public String back(Model model, int uid, HttpSession session,@RequestParam(value = "num", required = false, defaultValue = "1") int num){
        session.setAttribute("userIDD", uid);
        model.addAttribute("userIDD", uid);

        User userrr = usermapper.selectById(uid);

        model.addAttribute("Login_User", userrr);

        model.addAttribute("uuiui", userrr);

        IPage<Article> page = new Page<>(1, 8);


        QueryWrapper<attention> attentionQueryWrapper = new QueryWrapper<>();
        attentionQueryWrapper.eq("beUserId", uid);
        List<attention> attentions1 = attentionMapper.selectList(attentionQueryWrapper);
        model.addAttribute("count_size", attentions1.size());

//       根据ID查找
        QueryWrapper<Article> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userId", uid);

        queryWrapper.orderByDesc("create_time");

        IPage<Article> articles_page = articleMapper.selectPage(page, queryWrapper);
        List<Article> articles = articles_page.getRecords();

        model.addAttribute("page_count", articles_page.getCurrent());
        System.out.println("当前页码值：" + page.getCurrent());
        model.addAttribute("total_page", articles_page.getPages());
        System.out.println("总页数：" + page.getPages());

        model.addAttribute("articleList", articles);

        User user = usermapper.selectById(uid);
        model.addAttribute("user", user);

        Map<String, Object> Map_likes = usermapper.all_likes(uid);
        Object totalLikes = Map_likes.get("totalLikes");
        Object art_num = Map_likes.get("art_num");
        model.addAttribute("totalLikes", totalLikes);
        model.addAttribute("art_num", art_num);

        //未读点赞
        int likes = favorMapper.unread_num(uid);

        model.addAttribute("likes", likes);

        //最近评论数
        int comments = commentMapper.unread_num(uid);

        model.addAttribute("comments", comments);
//        最近关注
        int attention_num = attentionMapper.unattention_num(uid);

        model.addAttribute("attention_num", attention_num);


//        总访客数
        QueryWrapper<Visitor> queryWrapper_visitor = new QueryWrapper<>();
        queryWrapper_visitor.eq("visitorId", uid);
        queryWrapper_visitor.orderByDesc("create_time");
        List<Visitor> visitors = visitorMapper.selectList(queryWrapper_visitor);

        model.addAttribute("visitorList", visitors);


        model.addAttribute("visitorNum", visitors.size());
//      未读留言
        int message_num = messageMapper.unmessage_num(uid);
        model.addAttribute("message_num", message_num);
        model.addAttribute("num", num);



        return "back/personal";
    }

    //删除文章
    @RequestMapping("/article/delete")
    @ResponseBody
    public ResponseData delete(Integer id){
        System.out.println("触发删除");
        //逻辑删除
        articleMapper.deleteById(id);
        return ResponseData.OK();
    }
//    删除文章之后更新点赞和文章数
    @RequestMapping("/article/delete/update")
    @ResponseBody
    public String updateValue(HttpSession session){
        int uid=(int) session.getAttribute("userIDD");

        Map<String, Object> Map_likes = usermapper.all_likes(uid);
        Object totalLikes = Map_likes.get("totalLikes");
        Object art_num = Map_likes.get("art_num");


        return "{\"totalLikes\": \"" + totalLikes + "\", \"art_num\": \"" + art_num + "\"}";
    }
//    更新分页文章
    @RequestMapping("/article/page/{pageId}")
    public String page(@PathVariable("pageId")Integer pageId,Model model, HttpSession session){
        //获取文章的数据
        int uid=(int) session.getAttribute("userIDD");


        IPage<Article> page = new Page<>(pageId, 8);

//       根据ID查找
        QueryWrapper<Article> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userId", uid);
        IPage<Article> articles_page = articleMapper.selectPage(page, queryWrapper);
        List<Article> articles = articles_page.getRecords();

        model.addAttribute("articleList",articles);

        User user = usermapper.selectById(uid);
        model.addAttribute("user",user);
        return "back/page";
    }
//    更新分页按钮
    @RequestMapping("/article/page_num/{pageId}")
    public String page_num(@PathVariable("pageId")Integer pageId,Model model, HttpSession session){
        int uid=(int)session.getAttribute("userIDD");

        IPage<Article> page = new Page<>(pageId, 8);


//       根据ID查找
        QueryWrapper<Article> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userId", uid);
        IPage<Article> articles_page = articleMapper.selectPage(page, queryWrapper);
        List<Article> articles = articles_page.getRecords();

        model.addAttribute("page_count",articles_page.getCurrent());
        System.out.println("当前页码值："+page.getCurrent());
        model.addAttribute("total_page",articles_page.getPages());
        System.out.println("总页数："+page.getPages());
        return "back/page_num";
    }
    //    局部更新点赞

    @RequestMapping("/back/like")
    public String attentionInfo(Model model,HttpSession session){

//        根据uid查到所有的文章
        int uid=(int)session.getAttribute("userIDD");
//        int uid=1;

        QueryWrapper<Article> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userId", uid);
        List<Article> articles = articleMapper.selectList(queryWrapper);
//        查到文章之后再找点赞信息

        List <Favor> totalFavor = new ArrayList<>();

        for (Article article : articles) {
            QueryWrapper<Favor> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("articleId", article.getArticleId());
//            一篇文章的消息
            List<Favor> favors = favorMapper.selectList(queryWrapper1);
            totalFavor.addAll(favors);
        }
        totalFavor.sort((c1, c2) -> c2.getCreate_time().compareTo(c1.getCreate_time()));

        model.addAttribute("likes",totalFavor);
        return "back/personal_likes";
    }
    //    局部更新关注
    @RequestMapping("/back/attentions")
    public String likeInfo(Model model,HttpSession session){
        int uid=(int)session.getAttribute("userIDD");
        QueryWrapper<attention> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("beUserId", uid);
        queryWrapper.orderByDesc("create_time");
        List<attention> attentions = attentionMapper.selectList(queryWrapper);
        model.addAttribute("attentions",attentions);
        return "back/personal_attention";
    }
//局部更新评论
    @RequestMapping("/back/comment")
    public String commentInfo(Model model,HttpSession session){
        int uid=(int)session.getAttribute("userIDD");
        QueryWrapper<Article> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userId", uid);
        List<Article> articles = articleMapper.selectList(queryWrapper);

        List <Comment> totalcomments = new ArrayList<>();

        for (Article article : articles) {
            QueryWrapper<Comment> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("articleId", article.getArticleId());
//            一篇文章的消息
//            queryWrapper1.orderByDesc("create_time");
            List<Comment> comment = commentMapper.selectList(queryWrapper1);
            totalcomments.addAll(comment);
        }
        totalcomments.sort((c1, c2) -> c2.getCreate_time().compareTo(c1.getCreate_time()));
        model.addAttribute("comments",totalcomments);

        return "back/personal_comments";
    }
//    局部更新留言
    @RequestMapping("/back/message")
    public String messageInfo(Model model,HttpSession session){
        int uid=(int)session.getAttribute("userIDD");
        QueryWrapper<message> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("beUserId", uid);
        queryWrapper.orderByDesc("create_time");
        List<message> messages = messageMapper.selectList(queryWrapper);

        model.addAttribute("messages",messages);
        return "back/personal_message";
    }
    //    局部更新相册
    @RequestMapping("/back/picture")
    public String pictureInfo(Model model,HttpSession session){
        int uid=(int)session.getAttribute("userIDD");
        QueryWrapper<Picture> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userId", uid);
        queryWrapper.orderByDesc("create_time");
        List<Picture> pictures = pictureMapper.selectList(queryWrapper);

        model.addAttribute("pictures",pictures);
        return "back/personal_photo";
    }
    @RequestMapping("/back/visitor/picture")
    public String visitorpictureInfo(Model model,Integer visitorUserId){
        int uid=visitorUserId;

        QueryWrapper<Picture> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userId", uid);
        queryWrapper.orderByDesc("create_time");
        List<Picture> pictures = pictureMapper.selectList(queryWrapper);

        model.addAttribute("pictures",pictures);
        return "back/personal_photo";
    }
    @RequestMapping("/back/write")
    public String writeInfo(){
        return "back/write";
    }
    @RequestMapping("/back/visitor/write")
    public String writemessageInfo(Model model,Integer visitorId){
        model.addAttribute("visitorId",visitorId);
        return "back/message_write";
    }
//处理文章上传请求
    @RequestMapping("/back/articleSubmit")
    public String articleSubmit(@RequestParam("title") String title,
                                @RequestParam("article") String article,
                                @RequestPart("photos") MultipartFile[] photos,
                              Model model, HttpSession session) throws IOException{
        System.out.println("标题"+title);
        System.out.println("图片数量"+photos.length);


        int uid=(int)session.getAttribute("userIDD");
        model.addAttribute("userIDD", uid);

        IPage<Article> page = new Page<>(1, 8);


//       根据ID查找
        QueryWrapper<Article> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userId", uid);

        queryWrapper.orderByDesc("create_time");

        IPage<Article> articles_page = articleMapper.selectPage(page, queryWrapper);
        List<Article> articles = articles_page.getRecords();

        model.addAttribute("page_count", articles_page.getCurrent());
        System.out.println("当前页码值：" + page.getCurrent());
        model.addAttribute("total_page", articles_page.getPages());
        System.out.println("总页数：" + page.getPages());

        model.addAttribute("articleList", articles);

        User user = usermapper.selectById(uid);
        model.addAttribute("user", user);

        Map<String, Object> Map_likes = usermapper.all_likes(1);
        Object totalLikes = Map_likes.get("totalLikes");
        Object art_num = Map_likes.get("art_num");
        model.addAttribute("totalLikes", totalLikes);
        model.addAttribute("art_num", art_num);

//未读点赞
        int likes = favorMapper.unread_num(uid);

        model.addAttribute("likes", likes);

//        最近评论数
        int comments = commentMapper.unread_num(uid);

        model.addAttribute("comments", comments);
//        最近关注
        int attention_num = attentionMapper.unattention_num(uid);

        model.addAttribute("attention_num", attention_num);



//        总访客数
        QueryWrapper<Visitor> queryWrapper_visitor = new QueryWrapper<>();
        queryWrapper.eq("userId", uid);
        queryWrapper.orderByDesc("create_time");
        List<Visitor> visitors = visitorMapper.selectList(queryWrapper_visitor);
        model.addAttribute("visitorList", visitors);
        model.addAttribute("visitorNum", visitors.size());
//      未读留言
        int message_num = messageMapper.unmessage_num(uid);
        model.addAttribute("message_num", message_num);



        return "back/personal";

    }
    @RequestMapping("/back/test")
    public String back_test(Model model, HttpSession session) {
        int uid = (int) session.getAttribute("userIDD");

        model.addAttribute("userIDD", uid);

        IPage<Article> page = new Page<>(1, 8);


//       根据ID查找
        QueryWrapper<Article> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userId", uid);

        queryWrapper.orderByDesc("create_time");

        IPage<Article> articles_page = articleMapper.selectPage(page, queryWrapper);
        List<Article> articles = articles_page.getRecords();

        model.addAttribute("page_count", articles_page.getCurrent());
        System.out.println("当前页码值：" + page.getCurrent());
        model.addAttribute("total_page", articles_page.getPages());
        System.out.println("总页数：" + page.getPages());

        model.addAttribute("articleList", articles);

        User user = usermapper.selectById(uid);
        model.addAttribute("user", user);

        Map<String, Object> Map_likes = usermapper.all_likes(uid);
        Object totalLikes = Map_likes.get("totalLikes");
        Object art_num = Map_likes.get("art_num");
        model.addAttribute("totalLikes", totalLikes);
        model.addAttribute("art_num", art_num);

//未读点赞
        int likes = favorMapper.unread_num(uid);

        model.addAttribute("likes", likes);

//        最近评论数
        int comments = commentMapper.unread_num(uid);

        model.addAttribute("comments", comments);
//        最近关注
        int attention_num = attentionMapper.unattention_num(uid);

        model.addAttribute("attention_num", attention_num);



//        总访客数
        QueryWrapper<Visitor> queryWrapper_visitor = new QueryWrapper<>();
        queryWrapper_visitor.eq("userId", uid);
        queryWrapper_visitor.orderByDesc("create_time");
        List<Visitor> visitors = visitorMapper.selectList(queryWrapper_visitor);
        model.addAttribute("visitorList", visitors);
        model.addAttribute("visitorNum", visitors.size());
//      未读留言
        int message_num = messageMapper.unmessage_num(uid);
        model.addAttribute("message_num", message_num);



        return "back/personal";
    }
    @RequestMapping("/back/page/close")
    public String closepage(){
        return "back/close";
    }

    @RequestMapping("/back/submit")
    @ResponseBody
    public ResponseData submitArticle(String title,String Html,HttpSession session){
        int uid=(int)session.getAttribute("userIDD");
        Article article=new Article();

        article.setArtContent(Html);

        article.setTitle(title);

        article.setUserId(uid);

        article.setTag("标签");

        articleMapper.insert(article);

        return ResponseData.OK();
    }
    @RequestMapping("/back/visitor/submit")
    @ResponseBody
    public ResponseData submitmessage(String Html,Integer visitorId,HttpSession session){
        int uid=(int)session.getAttribute("userIDD");
        message messages=new message();
        messages.setMessage(Html);
        messages.setUserId(uid);
        messages.setBeuserId(visitorId);

        messageMapper.insert(messages);

        return ResponseData.OK();
    }
    @RequestMapping("/back/visitor/{visitorUserId}/{userIDD}")
    public String visitorPage(@PathVariable(value = "userIDD", required = false)Integer userIDD, @PathVariable(value = "visitorUserId", required = false)Integer visitorUserId,Model model, HttpSession session){

        int uid=visitorUserId;
        Integer ussis;
        if(userIDD != null){
            ussis = userIDD;
            session.setAttribute("userIDD", userIDD);
        }
        else {
            ussis = (Integer) session.getAttribute("userIDD");
        }



        model.addAttribute("userIDD", ussis);

        model.addAttribute("visitorUserId",visitorUserId);

        IPage<Article> page = new Page<>(1, 8);


//       根据ID查找
        QueryWrapper<Article> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userId", uid);

        queryWrapper.orderByDesc("create_time");

        IPage<Article> articles_page = articleMapper.selectPage(page, queryWrapper);
        List<Article> articles = articles_page.getRecords();

        model.addAttribute("page_count", articles_page.getCurrent());
        System.out.println("当前页码值：" + page.getCurrent());
        model.addAttribute("total_page", articles_page.getPages());
        System.out.println("总页数：" + page.getPages());

        model.addAttribute("articleList", articles);

        User user = usermapper.selectById(uid);
        model.addAttribute("user", user);

        Map<String, Object> Map_likes = usermapper.all_likes(uid);

        Object totalLikes ;
        Object art_num;
        if(Map_likes == null){
            totalLikes = 0;
            art_num = 0;
        }else{
            totalLikes = Map_likes.get("totalLikes");
            art_num = Map_likes.get("art_num");
        }

        model.addAttribute("totalLikes", totalLikes);
        model.addAttribute("art_num", art_num);


        model.addAttribute("attention", false);
        List<attention> userId = attentionMapper.selectList(new QueryWrapper<attention>().eq("userId", ussis));
        int flag = 0;
        for (attention attention :userId) {
            if(attention.getBeuserId().equals(uid)){
                flag = 1;
            }
        }
        if(flag == 1){
            model.addAttribute("attention", true);
        }else{
            model.addAttribute("attention", false);
        }


//        总访客数
        QueryWrapper<Visitor> queryWrapper_visitor = new QueryWrapper<>();
        queryWrapper_visitor.eq("userId", uid);
        queryWrapper_visitor.orderByDesc("create_time");
        List<Visitor> visitors = visitorMapper.selectList(queryWrapper_visitor);
        model.addAttribute("visitorList", visitors);
        model.addAttribute("visitorNum", visitors.size());
//      未读留言
        int message_num = messageMapper.unmessage_num(uid);
        model.addAttribute("message_num", message_num);

        if(userIDD != 0){
            //        如果最近访问过，则不插入
            QueryWrapper<Visitor> queryWrapper_notvisitor = new QueryWrapper<>();
            queryWrapper_notvisitor.eq("userId", userIDD);
            queryWrapper_notvisitor.eq("visitorId", visitorUserId);
            queryWrapper_notvisitor.orderByDesc("create_time");
            List<Visitor> allVisitor=visitorMapper.selectList(queryWrapper_notvisitor);
            if (!allVisitor.isEmpty()) {
                Visitor firstVisitor = allVisitor.get(0);
                Date nowDate = new Date();
                long differenceInMillis = nowDate.getTime() - firstVisitor.getCreate_time().getTime();
                if (differenceInMillis>=3600000){
                    Visitor visitor=new Visitor();
                    visitor.setVisitorId(visitorUserId);
                    visitor.setUserId(userIDD);
                    visitorMapper.insert(visitor);
                }
            }else{
                Visitor visitor=new Visitor();
                visitor.setVisitorId(visitorUserId);
                visitor.setUserId(userIDD);
                visitorMapper.insert(visitor);
            }
        }


        return "back/personal_visitor";
    }
//    将未读关注消息清零
    @RequestMapping("/back/closeAttentions")
    @ResponseBody
    public ResponseData closeAttentions(HttpSession session){
        int uid=(int)session.getAttribute("userIDD");
        attentionMapper.close_attention(uid);
        return ResponseData.OK();
    }
//    将未读点赞清零
    @RequestMapping("/back/closeLikes")
    @ResponseBody
    public ResponseData closeLikes(HttpSession session){
        int uid=(int)session.getAttribute("userIDD");
        favorMapper.close_favor(uid);
        return ResponseData.OK();
    }
    //    将未读评论清零
    @RequestMapping("/back/closeComments")
    @ResponseBody
    public ResponseData closeComments(HttpSession session){
        int uid=(int)session.getAttribute("userIDD");
        commentMapper.closeComments(uid);
        return ResponseData.OK();
    }
//    将未读留言清零
    @RequestMapping("/back/closeMessage")
    @ResponseBody
    public ResponseData closeMessage(HttpSession session){
        int uid=(int)session.getAttribute("userIDD");
        messageMapper.close_message(uid);
        return ResponseData.OK();
    }


    @GetMapping("/back/attentionup/{userIDD}")
    public String attention(@PathVariable(value = "userIDD", required = false) Integer userIDD, Integer Id, HttpSession session, Model model, Integer articleId){
        if(userIDD != null)
            session.setAttribute("userIDD", userIDD);
        if(userIDD != null && userIDD != 0){
            int flag = 0;
            QueryWrapper<attention> attentionQueryWrapper = new QueryWrapper<>();
            attentionQueryWrapper.eq("userId", userIDD);
            List<attention> attentions = attentionMapper.selectList(attentionQueryWrapper);
            for (attention attention :attentions) {
                if(attention.getBeuserId().equals(Id)){
                    flag = 1;
                }
            }

            if(flag == 1){
                QueryWrapper<attention> attentionQueryWrapper1 = new QueryWrapper<>();
                attentionQueryWrapper1.eq("userId", userIDD).eq("beUserId", Id);
                attentionMapper.delete(attentionQueryWrapper1);
            }else{
                attentionMapper.insert(new attention(userIDD, Id, 0));
            }

            return "redirect:/back/visitor/" + Id + "/" + userIDD;
        }
        return "redirect:/index";
    }

//    处理关注事件
//    @RequestMapping("'/back/attentionup")
//    public ResponseData attentionup(Integer visitorUserId,HttpSession session){
//        int uid=(int)session.getAttribute("userIDD");
//        attention attentions=new attention();
//        attentions.setUserId(visitorUserId);
//        attentions.setBeuserId(uid);
//        attentionMapper.insert(attentions);
//        return ResponseData.OK();
//    }
    @RequestMapping("/back/Article/test")
    public String Articlepage(){
        return "back/write/wangEditor_demo/index";
    }


    @RequestMapping("/text")
    public String textpage(){
        return "back/ajax_test";
    }
    @RequestMapping("/page1")
    public String textpage1(Model model){
        List<Article> articles= articleMapper.findAllarticle();
        model.addAttribute("articles",articles);

        model.addAttribute("articlesNum",4);//文章总数
        model.addAttribute("commentsNum",4);//评论总数

        model.addAttribute("comments",4);//最新评论

        System.out.println("触发了ajax");
        return "ajax_text/index";
    }











}
